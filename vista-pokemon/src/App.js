import './App.css';
import Form from './components/Form/Form';
import { Players } from './components/Players';

function App() {
  return (
    <div className="App">
      <Form/>
      <Players />
    </div>
  );
}

export default App;
