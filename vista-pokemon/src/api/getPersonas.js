import axios from "axios";
const headers = {
  'Accept': 'application/json',
  'Access-Control-Allow-Origin':'*',
  'Content-Type': 'application/json',
}

export const getPersonas = axios.get('https://jsonplaceholder.typicode.com/users', {
  credentials: 'include'
})