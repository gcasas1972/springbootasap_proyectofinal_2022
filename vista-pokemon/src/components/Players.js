import React, { useState, useEffect } from 'react'
import {ListGroup, Button, Card} from 'react-bootstrap'
import { getPersonas } from '../api/getPersonas'

export const Players = () => {

  const [usuarios, setUsuarios] = useState([]);

  const players = [
    {
      nombre: "Lucas",
      email: "lucas@mail.com",
      nickname: "luqui"
    },
    {
      nombre: "Marcos",
      email: "marcos@mail.com",
      nickname: "marquitos"
    },
    {
      nombre: "Matias",
      email: "matias@mail.com",
      nickname: "mati"
    }
  ]

  const credenciales = {
    nombre: 'Gabriel',
    clave: 'gcasas'
  }

  useEffect(() => {
    try {
      getPersonas.then(res => setUsuarios(res.data))
    } catch (error) {
      console.log("Error de mierda", error)
    }

  }, [])

  return (
    <div className="col md-6 mt-4">
      <ListGroup className='justify-content-center flex-row flex-wrap'>
        {
          usuarios.length == 0 && <p>Loading</p>
        }
        {
          usuarios.length > 0 &&
          usuarios.map((user, i) => (
            <ListGroup.Item key={i}>
            <Card style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Title>{user.nombre}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{user.username}</Card.Subtitle>
                <Card.Text>
                  {user.email}
                </Card.Text>
                <Button className="m-2">Modificar</Button>
                <Button className="m-2" variant="danger">Eliminar</Button>
              </Card.Body>
            </Card>
            </ListGroup.Item>
          ))
        }
    </ListGroup>
    </div>
  )
}

