import React from 'react'
import { Form, Button, Card } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function () {
    return (

    <div className='w-50 m-auto mt-4 mb-4'>
        <Form autoComplete='off'>
            <Card className='d-flex' style={{ border: 'none', flexDirection: 'row', justifyContent: 'space-around'}}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Nombre</Form.Label>
                    <Form.Control type="text" placeholder="ingrese nombre" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Nickname</Form.Label>
                    <Form.Control aria-autocomplete='none' type="text" placeholder="Enter email" />
                </Form.Group>
            </Card>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
            </Form.Group>
            <Button className="m-2" variant="primary" type="submit">
                Submit
            </Button>
            <Button className="m-2" variant="primary" type="submit">
                Buscar
            </Button>
        </Form>
    </div>
    )
}
