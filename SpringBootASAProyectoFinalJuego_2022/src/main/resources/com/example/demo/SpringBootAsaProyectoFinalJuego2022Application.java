package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAsaProyectoFinalJuego2022Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAsaProyectoFinalJuego2022Application.class, args);
	}

}
