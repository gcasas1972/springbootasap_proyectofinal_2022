package ar.edu.proyectoFinalJuego.modelo;

public class Arbol extends PiedraPapelTijeraFactory{
	public Arbol() {
		this("arbol", ARBOL);
	}
	public Arbol(String pNom , int pNum) {
		super (pNom, pNum);
	}
	
	@Override
	public boolean isMe(int pNum) {
		return pNum == ARBOL;
	}
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int result=0;
		switch(pPiedPapelTijera.getNumero()) {
		//gana con
			case DEMONIO:
			case DRAGON:
			case AGUA:
			case AIRE:
			case PAPEL:
			case ESPONJA:
			case LOBO:
				
				result=1;
				this.descripcionResultado = "Arbol le gana a " + pPiedPapelTijera.getNombre();
				break;
				
		//pierde con	
			case RAYO:
			case PISTOLA:
			case PIEDRA:
			case FUEGO:
			case TIJERA:
			case SERPIENTE:
			case HUMANO:
				
				result=-1;
				this.descripcionResultado = "Arbol le pierde a " + pPiedPapelTijera.getNombre();
				break;
			
			default:
				result=0;
				this.descripcionResultado = "Arbol le empata a " + pPiedPapelTijera.getNombre();
				break;
		}
		return result;
	}

}

