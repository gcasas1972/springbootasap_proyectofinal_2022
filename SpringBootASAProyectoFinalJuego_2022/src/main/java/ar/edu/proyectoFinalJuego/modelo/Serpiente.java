package ar.edu.proyectoFinalJuego.modelo;


public class Serpiente extends PiedraPapelTijeraFactory {

	public Serpiente() {
		this("serpiente", SERPIENTE);
	}
	
	public Serpiente(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		// TODO Auto-generated method stub
		return pNum == SERPIENTE;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO Auto-generated method stub
		int resul = 0;
		switch (pPiedPapelTijera.getNumero()) {
			//le gana a
			case HUMANO:
			case ARBOL:
			case LOBO:
			case ESPONJA:
			case PAPEL:
			case AIRE:
			case AGUA:
				resul=1;
				this.descripcionResultado = "Serpiente le gana a " + pPiedPapelTijera.getNombre();
				break ;
			//pierde con
			case TIJERA:
			case FUEGO:
			case PIEDRA:
			case PISTOLA:
			case RAYO:
			case DEMONIO:
			case DRAGON:
			    resul=-1;
			    this.descripcionResultado = "Serpiente le pierde a " + pPiedPapelTijera.getNombre();
			    break;
			default:			
				resul=0;
				this.descripcionResultado = "Serpiente le empata a " + pPiedPapelTijera.getNombre();
				break;
			}
			return resul;
	}
	
}