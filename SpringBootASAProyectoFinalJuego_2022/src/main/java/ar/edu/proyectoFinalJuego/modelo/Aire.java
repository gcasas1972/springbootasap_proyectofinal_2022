package ar.edu.proyectoFinalJuego.modelo;

public class Aire extends PiedraPapelTijeraFactory{
	public Aire() {
		this("aire", AIRE);
	}
	public Aire(String pNom , int pNum) {
		super (pNom, pNum);
	}
	
	@Override
	public boolean isMe(int pNum) {
		return pNum == AIRE;
	}
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int result=0;
		switch(pPiedPapelTijera.getNumero()) {
		//gana con
			case DRAGON:
			case DEMONIO:
			case RAYO:
			case PISTOLA:
			case PIEDRA:
			case FUEGO:
			case AGUA:
				
				result=1;
				this.descripcionResultado = "Aire le gana a " + pPiedPapelTijera.getNombre();
				break;
				
		//pierde con	
			case TIJERA:
			case SERPIENTE:
			case HUMANO:
			case ARBOL:
			case LOBO:
			case ESPONJA:
			case PAPEL:
				
				result=-1;
				this.descripcionResultado = "Aire le pierde a " + pPiedPapelTijera.getNombre();
				break;
			
			default:
				result=0;
				this.descripcionResultado = "Aire le empata a " + pPiedPapelTijera.getNombre();
				break;
		}
		return result;
	}
}
