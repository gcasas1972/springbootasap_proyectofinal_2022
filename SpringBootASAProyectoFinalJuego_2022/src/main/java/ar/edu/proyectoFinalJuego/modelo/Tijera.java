package ar.edu.proyectoFinalJuego.modelo;

public class Tijera extends PiedraPapelTijeraFactory {
	public Tijera() {
		this("tijera",TIJERA);
	}
	
	public Tijera(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==TIJERA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PAPEL:
		case SERPIENTE:
		case HUMANO:
		case ARBOL:
		case LOBO:
		case ESPONJA:
		case AIRE:
			resul=1;
			this.descripcionResultado = "tijera le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case PIEDRA:
        case FUEGO:
        case PISTOLA:
        case RAYO:
        case DEMONIO:
        case DRAGON:
        case AGUA:       
			resul=-1;
			this.descripcionResultado = "tijera le pierde a " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionResultado = "tijera le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
