package ar.edu.proyectoFinalJuego.modelo;

public class Rayo extends PiedraPapelTijeraFactory{
	public Rayo() {
		this("rayo", RAYO);
	}
	public Rayo(String pNom , int pNum) {
		super (pNom, pNum);
	}
	
	@Override
	public boolean isMe(int pNum) {
		return pNum == RAYO;
	}
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int result=0;
		switch(pPiedPapelTijera.getNumero()) {
		//gana con
			case PIEDRA:
			case FUEGO:
			case TIJERA:
			case SERPIENTE:
			case HUMANO:
			case ARBOL:
			case PISTOLA:
				
				result=1;
				this.descripcionResultado = "rayo le gana a " + pPiedPapelTijera.getNombre();
				break;
				
		//pierde con	
			case LOBO:
			case ESPONJA:
			case PAPEL:
			case AIRE:
			case AGUA:
			case DRAGON:
			case DEMONIO:
				
				result=-1;
				this.descripcionResultado = "rayo le pierde a " + pPiedPapelTijera.getNombre();
				break;
			
			default:
				result=0;
				this.descripcionResultado = "rayo le empata a " + pPiedPapelTijera.getNombre();
				break;
		}
		return result;
	}

}
