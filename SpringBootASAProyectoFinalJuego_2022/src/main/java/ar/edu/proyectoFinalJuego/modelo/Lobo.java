package ar.edu.proyectoFinalJuego.modelo;

public class Lobo extends PiedraPapelTijeraFactory {
	public Lobo() {
		this("lobo", LOBO);
	}
	
	public Lobo(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==LOBO;
	}
  
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
			case ESPONJA:
			case PAPEL:
			case AIRE:
			case AGUA:
			case DRAGON:
			case DEMONIO:
			case RAYO:
				resul=1;
				this.descripcionResultado = "lobo le gana a " + pPiedPapelTijera.getNombre();
			break;
			
	        case PISTOLA:
	        case PIEDRA:
	        case FUEGO:
	        case TIJERA:
	        case SERPIENTE:
	        case HUMANO:
	        case ARBOL:
	        	resul=-1;
	        	this.descripcionResultado = "lobo le pierde a " + pPiedPapelTijera.getNombre();
			break;

	        default:
	        	resul=0;
	        	this.descripcionResultado = "lobo le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}