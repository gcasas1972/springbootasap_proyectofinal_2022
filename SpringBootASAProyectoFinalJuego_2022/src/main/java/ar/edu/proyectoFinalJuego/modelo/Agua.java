package ar.edu.proyectoFinalJuego.modelo;

/**
 * @author 
 *
 *  El componenet e agua le gana a
 *  
 *  y pierde con 
 */
public class Agua extends PiedraPapelTijeraFactory {
	
	public Agua() {
		this("agua", AGUA );
	}

	public Agua(String pNom, int pNum) {
		super(pNom, pNum);		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==AGUA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		
		switch (pPiedPapelTijera.getNumero()) {
		case DRAGON:
		case DEMONIO:
		case RAYO:
		case PISTOLA:
		case PIEDRA:
		case FUEGO:
		case TIJERA:
			resul=1;
			this.descripcionResultado = "agua le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case PAPEL:
        case AIRE:
        case ESPONJA:
        case LOBO:
        case ARBOL:
        case HUMANO:
        case SERPIENTE:        	
			resul=-1;
			this.descripcionResultado = "agua le pierde a " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionResultado = "agua le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
