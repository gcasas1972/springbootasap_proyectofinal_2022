package ar.edu.proyectoFinalJuego.modelo;

public class Humano extends PiedraPapelTijeraFactory {
	public Humano() {
		this("humano", HUMANO);
	}
	
	public Humano(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==HUMANO;
	}
  
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
			case ARBOL:
			case DRAGON:
			case AGUA:
			case AIRE:
			case PAPEL:
			case ESPONJA:
			case LOBO:
				resul=1;
				this.descripcionResultado = "humano le gana a " + pPiedPapelTijera.getNombre();
			break;
			
	        case TIJERA:
	        case FUEGO:
	        case PIEDRA:
	        case PISTOLA:
	        case RAYO:
	        case DEMONIO:
	        case SERPIENTE:
	        	resul=-1;
	        	this.descripcionResultado = "humano le pierde a " + pPiedPapelTijera.getNombre();
			break;

	        default:
	        	resul=0;
	        	this.descripcionResultado = "humano le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
