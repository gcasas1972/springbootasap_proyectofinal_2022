package ar.edu.proyectoFinalJuego.modelo;

public class Piedra extends PiedraPapelTijeraFactory {
	public Piedra() {
		this("piedra", PIEDRA );
	}

	public Piedra(String pNom, int pNum) {
		super(pNom, pNum);		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case TIJERA:
		case FUEGO: 
		case SERPIENTE:
		case HUMANO:
		case ARBOL:
		case LOBO: 
		case ESPONJA: 
			resul=1;
			this.descripcionResultado = "piedra le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case PAPEL:
        case PISTOLA:
        case RAYO:
        case DEMONIO:
        case DRAGON:
        case AGUA:
        case AIRE:
			resul=-1;
			this.descripcionResultado = "piedra le pierde a " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionResultado = "piedra le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
