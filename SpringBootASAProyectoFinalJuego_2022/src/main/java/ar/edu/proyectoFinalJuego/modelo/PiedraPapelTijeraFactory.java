package ar.edu.proyectoFinalJuego.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {
	
	//Gaston PIEDRA PAPEL TIJERA AGUA
	public final static int PIEDRA 		= 1;
	public final static int PAPEL 		= 2;
	public final static int TIJERA 		= 3;
	
	//Mauro PISTOLA FUEGO LOBO
	public final static int PISTOLA 	= 4;
	public final static int RAYO 		= 5;
	public final static int DEMONIO 	= 6;
	
	//Neyen DRAGON ESPONJA SERPIENTE
	public final static int DRAGON 		= 7;
	public final static int AGUA 		= 8;
	public final static int AIRE 		= 9;
	
	//Lucia DEMONIO HUMANO
	public final static int LOBO		= 10;
	public final static int ESPONJA 	= 11;
	public final static int ARBOL 		= 12;
	
	//Natacha RAYO AIRE ARBOL
	public final static int HUMANO 		= 13;
	public final static int FUEGO 		= 14;
	public final static int SERPIENTE 	= 15;
	
	//atributos
	protected String 								descripcionResultado; // CORREGIR::: descripcionResultado
	private static List<PiedraPapelTijeraFactory> 	elementos			;
	protected String 								nombre				;
	protected int 									numero				;

	//constructores
	public PiedraPapelTijeraFactory(String pNom, int pNum) {
		nombre = pNom;
		numero = pNum;
	}
	
	//gettters y setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}
	
	//metodo de negocio
	public abstract boolean isMe(int pNum);
	public abstract int comparar(PiedraPapelTijeraFactory pPiedPapelTijera);
	
	public static PiedraPapelTijeraFactory getInstance(int pNumero) {

		//el corazon del factory, 1ro el padre reconoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());		
		elementos.add(new Pistola());
		elementos.add(new Rayo());
		elementos.add(new Demonio());
		elementos.add(new Dragon());
		elementos.add(new Agua());
		elementos.add(new Aire());		
		elementos.add(new Lobo());
		elementos.add(new Esponja());
		elementos.add(new Arbol());
		elementos.add(new Humano());
		elementos.add(new Fuego());
		elementos.add(new Serpiente());		
		
		//es todo el codigo va a ser siempre el mismo
		for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if(piedraPapelTijeraFactory.isMe(pNumero))
				return piedraPapelTijeraFactory;	 	
		}
				
		return null;
	}

	@Override
	public String toString() {

		return this.numero + "-" + this.nombre;
	}	
}