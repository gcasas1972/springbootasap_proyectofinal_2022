package ar.edu.proyectoFinalJuego.modelo.test;

import static org.junit.jupiter.api.Assertions.assertEquals; 

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import ar.edu.proyectoFinalJuego.modelo.PiedraPapelTijeraFactory;
import ar.edu.proyectoFinalJuego.modelo.Piedra;
import ar.edu.proyectoFinalJuego.modelo.Papel;
import ar.edu.proyectoFinalJuego.modelo.Tijera;
import ar.edu.proyectoFinalJuego.modelo.Pistola;
import ar.edu.proyectoFinalJuego.modelo.Rayo;
import ar.edu.proyectoFinalJuego.modelo.Demonio;
import ar.edu.proyectoFinalJuego.modelo.Dragon;
import ar.edu.proyectoFinalJuego.modelo.Agua;
import ar.edu.proyectoFinalJuego.modelo.Aire;
import ar.edu.proyectoFinalJuego.modelo.Lobo;
import ar.edu.proyectoFinalJuego.modelo.Esponja;
import ar.edu.proyectoFinalJuego.modelo.Arbol;
import ar.edu.proyectoFinalJuego.modelo.Humano;
import ar.edu.proyectoFinalJuego.modelo.Fuego;
import ar.edu.proyectoFinalJuego.modelo.Serpiente;

class PiedraPapelTijeraFactoryTest {
	
	//lote de pruebas
	PiedraPapelTijeraFactory piedra, papel, tijera, pistola, rayo, demonio, dragon, 
					   agua, aire, lobo, esponja, arbol, humano, fuego, serpiente;

	@BeforeEach
	void setUp() throws Exception {
		
		//se ejecuta antes de cada prueba
		piedra 	  = new Piedra()   ;
		papel 	  = new Papel()	   ;
		tijera 	  = new Tijera()   ;
		pistola	  = new Pistola()  ;  		
		rayo 	  = new Rayo()	   ;
		demonio   = new Demonio()  ;
		dragon	  = new Dragon()   ;
		agua      = new Agua()     ;
		aire	  = new Aire()	   ;
		lobo	  = new Lobo()	   ;
		esponja	  = new Esponja()  ;		
		arbol	  = new Arbol()	   ;
		humano    = new Humano()   ;		
		fuego	  = new Fuego()    ;
		serpiente = new Serpiente();
	}

	@AfterEach
	void tearDown() throws Exception {

		piedra    = null;
		papel     = null;
		tijera    = null;
		pistola   = null;
		rayo      = null;
		demonio   = null;
		dragon    = null;
		agua      = null;
		aire      = null;
		lobo      = null;
		esponja   = null;
		arbol     = null;
		humano    = null;
		fuego     = null;
		serpiente = null;

	}

	@Test
	void testGetInstancePiedra() {
		assertEquals("piedra",
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
									   .getNombre()
									   .toLowerCase());
	}

	@Test
	void testGetInstancePapel() {
		assertEquals("papel",
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL)
				                       .getNombre()
				                       .toLowerCase());
	}

	@Test
	void testGetInstanceTiera() {
		assertEquals("tijera",
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA)
				                       .getNombre()
				                       .toLowerCase());
	}
	
	@Test
	void testGetInstancePistola() {
		assertEquals("pistola", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PISTOLA)
									   .getNombre()
									   .toLowerCase());		
	}
	
	@Test
	void testGetInstanceRayo() {
		assertEquals("rayo", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.RAYO)
				                       .getNombre()
				                       .toLowerCase());
	}
	
	@Test
	void testGetInstanceDemonio() {
		assertEquals("demonio",
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.DEMONIO)
				                       .getNombre()
				                       .toLowerCase());
	}
	
	@Test
	void testGetInstanceDragon() {
		assertEquals("dragon", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.DRAGON)
									   .getNombre()
						               .toLowerCase());		
	}
	
	@Test
	void testGetInstanceAgua() {
		assertEquals("agua", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.AGUA)
									   .getNombre()
									   .toLowerCase());		
	}	
	
	@Test
	void testGetInstanceAire() {
		assertEquals("aire", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.AIRE)
									   .getNombre()
									   .toLowerCase());
	}
	
	@Test
	void testGetInstanceLobo() {
		assertEquals("lobo", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LOBO)
    								   .getNombre()
									   .toLowerCase());		
	}
	
	@Test
	void testGetInstanceEsponja() {
		assertEquals("esponja", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.ESPONJA)
									   .getNombre()
									   .toLowerCase());		
	}
	
	@Test
	void testGetInstanceArbol() {
		assertEquals("arbol", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.ARBOL)
								       .getNombre()
									   .toLowerCase());		
	}	
	
	@Test
	void testGetInstanceHumano() {
		assertEquals("humano",
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.HUMANO)
				                       .getNombre()
				                       .toLowerCase());
	}
	
	void testGetInstanceFuego() {
		assertEquals("fuego", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.FUEGO)
							           .getNombre()
									   .toLowerCase());		
	}

	@Test
	void testGetInstanceSerpiente() {
		assertEquals("serpiente", 
				PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SERPIENTE)
								       .getNombre()
									   .toLowerCase());		
	}
		
	//Casos de PIEDRA le gana a
	
	@Test
	void testCompararPiedraGanaATijera() {
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("piedra le gana a tijera", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaAFuego() {
		assertEquals(1, piedra.comparar(fuego));
		assertEquals("piedra le gana a fuego", piedra.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaASerpiente() {
		assertEquals(1, piedra.comparar(serpiente));
		assertEquals("piedra le gana a serpiente", piedra.getDescripcionResultado()
													     .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaAArbol() {
		assertEquals(1, piedra.comparar(arbol));
		assertEquals("piedra le gana a arbol", piedra.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaALobo() {
		assertEquals(1, piedra.comparar(lobo));
		assertEquals("piedra le gana a lobo", piedra.getDescripcionResultado()
													.toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaAEsponja() {
		assertEquals(1, piedra.comparar(esponja));
		assertEquals("piedra le gana a esponja", piedra.getDescripcionResultado()
													   .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaAHumano() {
		assertEquals(1, piedra.comparar(humano));
		assertEquals("piedra le gana a humano", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//Casos de PIEDRA le pierde a
	
	@Test
	void testCompararPiedraPierdeAPapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("piedra le pierde a papel", piedra.getDescripcionResultado()
													   .toLowerCase());
	}
	
	@Test
	void testCompararPiedraPierdeAPistola() {
		assertEquals(-1, piedra.comparar(pistola));
		assertEquals("piedra le pierde a pistola", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraPierdeARayo() {
		assertEquals(-1, piedra.comparar(rayo));
		assertEquals("piedra le pierde a rayo", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraPierdeADemonio() {
		assertEquals(-1, piedra.comparar(demonio));
		assertEquals("piedra le pierde a demonio", piedra.getDescripcionResultado()
													     .toLowerCase());
	}
	
	@Test
	void testCompararPiedraPierdeADragon() {
		assertEquals(-1, piedra.comparar(dragon));
		assertEquals("piedra le pierde a dragon", piedra.getDescripcionResultado()
													    .toLowerCase());
	}
	
	@Test
	void testCompararPiedraPierdeAAgua() {
		assertEquals(-1, piedra.comparar(agua));
		assertEquals("piedra le pierde a agua", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraPierdeAAire() {
		assertEquals(-1, piedra.comparar(aire));
		assertEquals("piedra le pierde a aire", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//Casos de PIEDRA le empata a
	
	@Test
	void testCompararPiedraEmpataAPiedra() {
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("piedra le empata a piedra", piedra.getDescripcionResultado()
													    .toLowerCase());
	}

	//Casos de PAPEL le gana a
	
	@Test
	void testCompararPapelGanaAPiedra() {
		assertEquals(1, papel.comparar(piedra));
		assertEquals("papel le gana a piedra", papel.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaAPistola() {
		assertEquals(1, papel.comparar(pistola));
		assertEquals("papel le gana a pistola", papel.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaARayo() {
		assertEquals(1, papel.comparar(rayo));
		assertEquals("papel le gana a rayo", papel.getDescripcionResultado()
												  .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaADemonio() {
		assertEquals(1, papel.comparar(demonio));
		assertEquals("papel le gana a demonio", papel.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaADragon() {
		assertEquals(1, papel.comparar(dragon));
		assertEquals("papel le gana a dragon", papel.getDescripcionResultado()
													.toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaAAgua() {
		assertEquals(1, papel.comparar(agua));
		assertEquals("papel le gana a agua", papel.getDescripcionResultado()
												  .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaAAire() {
		assertEquals(1, papel.comparar(aire));
		assertEquals("papel le gana a aire", papel.getDescripcionResultado()
												  .toLowerCase());
	}
	
	//Casos de PAPEL le pierde a
	
	@Test
	void testCompararPapelPierdeATijera() {
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel le pierde a tijera", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeAFuego() {
		assertEquals(-1, papel.comparar(fuego));
		assertEquals("papel le pierde a fuego", papel.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeASerpiente() {
		assertEquals(-1, papel.comparar(serpiente));
		assertEquals("papel le pierde a serpiente", papel.getDescripcionResultado()
													     .toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeAHumano() {
		assertEquals(-1, papel.comparar(humano));
		assertEquals("papel le pierde a humano", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeAArbol() {
		assertEquals(-1, papel.comparar(arbol));
		assertEquals("papel le pierde a arbol", papel.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeALobo() {
		assertEquals(-1, papel.comparar(lobo));
		assertEquals("papel le pierde a lobo", papel.getDescripcionResultado()
													.toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeAEsponja() {
		assertEquals(-1, papel.comparar(esponja));
		assertEquals("papel le pierde a esponja", papel.getDescripcionResultado()
													   .toLowerCase());
	}
	
	//Casos de PAPEL le empata a
	
	@Test
	void testCompararPapelEmpataConPapel() {
		assertEquals(0, papel.comparar(papel));
		assertEquals("papel le empata a papel", papel.getDescripcionResultado()
													  .toLowerCase());
	}

	//Casos de TIJERA le gana a
	
	@Test
	void testCompararTijeraGanaAPapel() {
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera le gana a papel", tijera.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaASerpiente() {
		assertEquals(1, tijera.comparar(serpiente));
		assertEquals("tijera le gana a serpiente", tijera.getDescripcionResultado()
													     .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaAHumano() {
		assertEquals(1, tijera.comparar(humano));
		assertEquals("tijera le gana a humano", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaAArbol() {
		assertEquals(1, tijera.comparar(arbol));
		assertEquals("tijera le gana a arbol", tijera.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaALobo() {
		assertEquals(1, tijera.comparar(lobo));
		assertEquals("tijera le gana a lobo", tijera.getDescripcionResultado()
													.toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaAEsponja() {
		assertEquals(1, tijera.comparar(esponja));
		assertEquals("tijera le gana a esponja", tijera.getDescripcionResultado()
													   .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaAAire() {
		assertEquals(1, tijera.comparar(aire));
		assertEquals("tijera le gana a aire", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//Casos de TIJERA le pierde a
	
	@Test
	void testCompararTijeraPierdeAPiedra() {
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("tijera le pierde a piedra", tijera.getDescripcionResultado()
													    .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeAFuego() {
		assertEquals(-1, tijera.comparar(fuego));
		assertEquals("tijera le pierde a fuego", tijera.getDescripcionResultado()
													   .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeAPistola() {
		assertEquals(-1, tijera.comparar(pistola));
		assertEquals("tijera le pierde a pistola", tijera.getDescripcionResultado()
													     .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeARayo() {
		assertEquals(-1, tijera.comparar(rayo));
		assertEquals("tijera le pierde a rayo", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeADemonio() {
		assertEquals(-1, tijera.comparar(demonio));
		assertEquals("tijera le pierde a demonio", tijera.getDescripcionResultado()
													     .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeADragon() {
		assertEquals(-1, tijera.comparar(dragon));
		assertEquals("tijera le pierde a dragon", tijera.getDescripcionResultado()
													    .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeConAgua() {
		assertEquals(-1, tijera.comparar(agua));
		assertEquals("tijera le pierde a agua", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//Casos de TIJERA le empata a
	
	@Test
	void testCompararTijeraEmpataATijera() {
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("tijera le empata a tijera", tijera.getDescripcionResultado()
													    .toLowerCase());
	}	

	//Casos de PISTOLA le gana a

	@Test
	void testCompararPistolaGanaALobo() {
		assertEquals(1, pistola.comparar(lobo));
		assertEquals("pistola le gana a lobo", pistola.getDescripcionResultado()
				                                      .toLowerCase());
	}

	@Test
	void testCompararPistolaGanaAArbol() {
		assertEquals(1, pistola.comparar(arbol));
		assertEquals("pistola le gana a arbol", pistola.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararPistolaGanaADragonHumano() {
		assertEquals(1, pistola.comparar(humano));
		assertEquals("pistola le gana a humano", pistola.getDescripcionResultado()
				                                        .toLowerCase());
	}

	@Test
	void testCompararPistolaGanaASerpiente() {
		assertEquals(1, pistola.comparar(serpiente));
		assertEquals("pistola le gana a serpiente", pistola.getDescripcionResultado()
				                                           .toLowerCase());
	}

	@Test
	void testCompararPistolaGanaATijera() {
		assertEquals(1, pistola.comparar(tijera));
		assertEquals("pistola le gana a tijera", pistola.getDescripcionResultado()
				                                        .toLowerCase());
	}

	@Test
	void testCompararPistolaGanaAFuego() {
		assertEquals(1, pistola.comparar(fuego));
		assertEquals("pistola le gana a fuego", pistola.getDescripcionResultado()
				                                       .toLowerCase());
	}

	@Test
	void testCompararPistolaGanaAPiedra() {
		assertEquals(1, pistola.comparar(piedra));
		assertEquals("pistola le gana a piedra", pistola.getDescripcionResultado()
				                                        .toLowerCase());
	}
	
	//Casos de PISTOLA le pierde a

	@Test
	void testCompararPistolaPierdeARayo() {
		assertEquals(-1, pistola.comparar(rayo));
		assertEquals("pistola le pierde a rayo", pistola.getDescripcionResultado()
				                                        .toLowerCase());
	}

	@Test
	void testCompararPistolaPierdeADemonio() {
		assertEquals(-1, pistola.comparar(demonio));
		assertEquals("pistola le pierde a demonio", pistola.getDescripcionResultado()
				                                           .toLowerCase());
	}

	@Test
	void testCompararPistolaPierdeADragon() {
		assertEquals(-1, pistola.comparar(dragon));
		assertEquals("pistola le pierde a dragon", pistola.getDescripcionResultado()
				                                          .toLowerCase());
	}

	@Test
	void testCompararPistolaPierdeAAgua() {
		assertEquals(-1, pistola.comparar(agua));
		assertEquals("pistola le pierde a agua", pistola.getDescripcionResultado()
				                                        .toLowerCase());
	}

	@Test
	void testCompararPistolaPierdeAAire() {
		assertEquals(-1, pistola.comparar(aire));
		assertEquals("pistola le pierde a aire", pistola.getDescripcionResultado()
				                                        .toLowerCase());
	}

	@Test
	void testCompararPistolaPierdeAEsponja() {
		assertEquals(-1, pistola.comparar(esponja));
		assertEquals("pistola le pierde a esponja", pistola.getDescripcionResultado()
				                                           .toLowerCase());
	}

	@Test
	void testCompararPistolaPierdeConPapel() {
		assertEquals(-1, pistola.comparar(papel));
		assertEquals("pistola le pierde a papel", pistola.getDescripcionResultado()
				                                         .toLowerCase());
	}

	//Casos de PISTOLA le empata a
	@Test
	void testCompararPistolaEmpataAPistola() {
		assertEquals(0, pistola.comparar(pistola));
		assertEquals("pistola le empata a pistola", pistola.getDescripcionResultado()
				                                           .toLowerCase());
	}
	
	//Casos de RAYO le gana a
	
 	@Test
	void testCompararRayoGanaAPiedra() {
		assertEquals(1, rayo.comparar(piedra));
		assertEquals("rayo le gana a piedra", rayo.getDescripcionResultado()
											      .toLowerCase());
	}
	
	@Test
	void testCompararRayoGanaAFuego() {
		assertEquals(1, rayo.comparar(fuego));
		assertEquals("rayo le gana a fuego", rayo.getDescripcionResultado()
												 .toLowerCase());
	}
	
	@Test
	void testCompararRayoGanaATijera() {
		assertEquals(1, rayo.comparar(tijera));
		assertEquals("rayo le gana a tijera", rayo.getDescripcionResultado()
												  .toLowerCase());
	}
	
	@Test
	void testCompararRayoGanaASerpiente() {
		assertEquals(1, rayo.comparar(serpiente));
		assertEquals("rayo le gana a serpiente", rayo.getDescripcionResultado()
													 .toLowerCase());
	}
	
	@Test
	void testCompararRayoGanaAHumano() {
		assertEquals(1, rayo.comparar(humano));
		assertEquals("rayo le gana a humano", rayo.getDescripcionResultado()
												  .toLowerCase());
	}
	
	@Test
	void testCompararRayoGanaAArbol() {
		assertEquals(1, rayo.comparar(arbol));
		assertEquals("rayo le gana a arbol", rayo.getDescripcionResultado()
												 .toLowerCase());
	}
	
	@Test
	void testCompararRayoGanaAPistola() {
		assertEquals(1, rayo.comparar(pistola));
		assertEquals("rayo le gana a pistola", rayo.getDescripcionResultado()
												   .toLowerCase());
	}
	
    //Casos de RAYO le pierde a
	
	@Test
	void testCompararRayoPierdeConLobo() {
		assertEquals(-1, rayo.comparar(lobo));
		assertEquals("rayo le pierde a lobo", rayo.getDescripcionResultado()
											      .toLowerCase());
	}
	
	@Test
	void testCompararRayoPierdeConEsponja() {

		assertEquals(-1, rayo.comparar(esponja));
		assertEquals("rayo le pierde a esponja", rayo.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararRayoPierdeConPapel() {

		assertEquals(-1, rayo.comparar(papel));
		assertEquals("rayo le pierde a papel", rayo.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararRayoPierdeConAire() {

		assertEquals(-1, rayo.comparar(aire));
		assertEquals("rayo le pierde a aire", rayo.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararRayoPierdeConAgua() {

		assertEquals(-1, rayo.comparar(agua));
		assertEquals("rayo le pierde a agua", rayo.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararRayoPierdeCoDragon() {

		assertEquals(-1, rayo.comparar(dragon));
		assertEquals("rayo le pierde a dragon", rayo.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararRayoPierdeConDemonio() {

		assertEquals(-1, rayo.comparar(demonio));
		assertEquals("rayo le pierde a demonio", rayo.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//RAYO EMPATA
	@Test
	void testCompararRayoEmpataConRayo() {

		assertEquals(0, rayo.comparar(rayo));
		assertEquals("rayo le empata a rayo", rayo.getDescripcionResultado()
													  .toLowerCase());
	}
	
	// Casos de DEMONIO

	@Test
	void testCompararDemonioaGanaARayo() {

		assertEquals(1, demonio.comparar(rayo));
		assertEquals("demonio le gana a rayo", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioaGanaAPiedra() {

		assertEquals(1, demonio.comparar(piedra));
		assertEquals("demonio le gana a piedra", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioaGanaATijera() {

		assertEquals(1, demonio.comparar(tijera));
		assertEquals("demonio le gana a tijera", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioaGanaASerpiente() {

		assertEquals(1, demonio.comparar(serpiente));
		assertEquals("demonio le gana a serpiente", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioaGanaAHumano() {

		assertEquals(1, demonio.comparar(humano));
		assertEquals("demonio le gana a humano", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioaGanaAFuego() {

		assertEquals(1, demonio.comparar(fuego));
		assertEquals("demonio le gana a fuego", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioaGanaAPistola() {

		assertEquals(1, demonio.comparar(pistola));
		assertEquals("demonio le gana a pistola", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioPierdeLobo() {
		assertEquals(-1, demonio.comparar(lobo));
		assertEquals("demonio le pierde a lobo", demonio.getDescripcionResultado().toLowerCase());
	}
	
	
	@Test
	void testCompararDemonioPierdeConEsponja() {
		assertEquals(-1, demonio.comparar(esponja));
		assertEquals("demonio le pierde a esponja", demonio.getDescripcionResultado().toLowerCase());
	}
	
	@Test
	void testCompararDemonioPierdeConPapel() {
		assertEquals(-1, demonio.comparar(arbol));
		assertEquals("demonio le pierde a arbol", demonio.getDescripcionResultado().toLowerCase());
	}
	
	@Test
	void testCompararDemonioPierdeConArbol() {
		assertEquals(-1, demonio.comparar(papel));
		assertEquals("demonio le pierde a papel", demonio.getDescripcionResultado().toLowerCase());
	}
	
	@Test
	void testCompararDemonioPierdeConAire() {
		assertEquals(-1, demonio.comparar(aire));
		assertEquals("demonio le pierde a aire", demonio.getDescripcionResultado().toLowerCase());
	}
	
	@Test
	void testCompararDemonioPierdeConAgua() {
		assertEquals(-1, demonio.comparar(agua));
		assertEquals("demonio le pierde a agua", demonio.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararDemonioPierdeConDragon() {
		assertEquals(-1, demonio.comparar(dragon));
		assertEquals("demonio le pierde a dragon", demonio.getDescripcionResultado().toLowerCase());
	}
	
	//DEMONIO EMPATA
	@Test
	void testCompararDemonioEmpataConDemonio() {

		assertEquals(0, demonio.comparar(demonio));
		assertEquals("demonio le empata a demonio", demonio.getDescripcionResultado()
													  .toLowerCase());
	}
	
	// Casos de DRAGON
	
	// gana
	@Test
	void testCompararDragonGanaADemonio() {
		assertEquals(1, dragon.comparar(demonio));
		assertEquals("dragon le gana a demonio", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaARayo() {
		assertEquals(1, dragon.comparar(rayo));
		assertEquals("dragon le gana a rayo", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaAPistola() {
		assertEquals(1, dragon.comparar(pistola));
		assertEquals("dragon le gana a pistola", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaAPiedra() {
		assertEquals(1, dragon.comparar(piedra));
		assertEquals("dragon le gana a piedra", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaAFuego() {
		assertEquals(1, dragon.comparar(fuego));
		assertEquals("dragon le gana a fuego", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaATijeras() {
		assertEquals(1, dragon.comparar(tijera));
		assertEquals("dragon le gana a tijera", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaASerpiente() {
		assertEquals(1, dragon.comparar(serpiente));
		assertEquals("dragon le gana a serpiente", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	// perdio
	
	@Test
	void testCompararDragonPierdeConHumano() {
		assertEquals(-1, dragon.comparar(humano));
		assertEquals("dragon le pierde a humano", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConArbol() {
		assertEquals(-1, dragon.comparar(arbol));
		assertEquals("dragon le pierde a arbol", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConLobo() {
		assertEquals(-1, dragon.comparar(lobo));
		assertEquals("dragon le pierde a lobo", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConEsponja() {
		assertEquals(-1, dragon.comparar(esponja));
		assertEquals("dragon le pierde a esponja", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConPapel() {
		assertEquals(-1, dragon.comparar(papel));
		assertEquals("dragon le pierde a papel", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConAire() {
		assertEquals(-1, dragon.comparar(aire));
		assertEquals("dragon le pierde a aire", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConAgua() {
		assertEquals(-1, dragon.comparar(agua));
		assertEquals("dragon le pierde a agua", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	// empato
	
	@Test
	void testCompararDragonEmpataConDragon() {
		assertEquals(0, dragon.comparar(dragon));
		assertEquals("dragon le empata a dragon", dragon.getDescripcionResultado()
													  .toLowerCase());
	}
	
	 //Casos de AGUA
	
	@Test
	void testCompararAguaGanaADragon() {
		assertEquals(1, agua.comparar(dragon));
		assertEquals("agua le gana a dragon", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaADemonio() {
		assertEquals(1, agua.comparar(demonio));
		assertEquals("agua le gana a demonio", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaARayo() {
		assertEquals(1, agua.comparar(rayo));
		assertEquals("agua le gana a rayo", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaAPistola() {
		assertEquals(1, agua.comparar(pistola));
		assertEquals("agua le gana a pistola", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaAPiedra() {
		assertEquals(1, agua.comparar(piedra));
		assertEquals("agua le gana a piedra", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaAFuego() {
		assertEquals(1, agua.comparar(fuego));
		assertEquals("agua le gana a fuego", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaATijera() {
		assertEquals(1, agua.comparar(tijera));
		assertEquals("agua le gana a tijera", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConAire() {
		assertEquals(-1, agua.comparar(aire));
		assertEquals("agua le pierde a aire", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConPapel() {
		assertEquals(-1, agua.comparar(papel));
		assertEquals("agua le pierde a papel", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConEsponja() {
		assertEquals(-1, agua.comparar(esponja));
		assertEquals("agua le pierde a esponja", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConLobo() {
		assertEquals(-1, agua.comparar(lobo));
		assertEquals("agua le pierde a lobo", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConArbol() {
		assertEquals(-1, agua.comparar(arbol));
		assertEquals("agua le pierde a arbol", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConHumano() {
		assertEquals(-1, agua.comparar(humano));
		assertEquals("agua le pierde a humano", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConSerpiente() {
		assertEquals(-1, agua.comparar(serpiente));
		assertEquals("agua le pierde a serpiente", agua.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAguaEmpataConAgua() {
		assertEquals(0, agua.comparar(agua));
		assertEquals("agua le empata a agua", agua.getDescripcionResultado()
													  .toLowerCase());
	}	
	
	//CASOS AIRE
	//AIRE GANA CON...
	
	@Test
	void testCompararAireGanaADragon() {

		assertEquals(1, aire.comparar(dragon));
		assertEquals("aire le gana a dragon", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	
	@Test
	void testCompararAireGanaADemonio() {

		assertEquals(1, aire.comparar(demonio));
		assertEquals("aire le gana a demonio", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAireGanaARayo() {

		assertEquals(1, aire.comparar(rayo));
		assertEquals("aire le gana a rayo", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAireGanaAPistola() {

		assertEquals(1, aire.comparar(pistola));
		assertEquals("aire le gana a pistola", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAireGanaAPiedra() {

		assertEquals(1, aire.comparar(piedra));
		assertEquals("aire le gana a piedra", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAireGanaAFuego() {

		assertEquals(1, aire.comparar(fuego));
		assertEquals("aire le gana a fuego", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAireGanaAAgua() {

		assertEquals(1, aire.comparar(agua));
		assertEquals("aire le gana a agua", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//AIRE PIERDE CON ...
	
	@Test
	void testCompararAirePierdeConTijera() {

		assertEquals(-1, aire.comparar(tijera));
		assertEquals("aire le pierde a tijera", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConSerpiente() {

		assertEquals(-1, aire.comparar(serpiente));
		assertEquals("aire le pierde a serpiente", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConHumano() {

		assertEquals(-1, aire.comparar(humano));
		assertEquals("aire le pierde a humano", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConArbol() {

		assertEquals(-1, aire.comparar(arbol));
		assertEquals("aire le pierde a arbol", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConLobo() {

		assertEquals(-1, aire.comparar(lobo));
		assertEquals("aire le pierde a lobo", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConEsponja() {

		assertEquals(-1, aire.comparar(esponja));
		assertEquals("aire le pierde a esponja", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConPapel() {

		assertEquals(-1, aire.comparar(papel));
		assertEquals("aire le pierde a papel", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//AIRE EMPATA
	@Test
	void testCompararAireEmpataConAire() {

		assertEquals(0, aire.comparar(aire));
		assertEquals("aire le empata a aire", aire.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//Casos de LOBO
	@Test
	void testCompararLoboGanaAEsponja() {
		assertEquals(1, lobo.comparar(esponja));
		assertEquals("lobo le gana a esponja", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboGanaAPapel() {

		assertEquals(1, lobo.comparar(papel));
		assertEquals("lobo le gana a papel", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboGanaAAire() {

		assertEquals(1, lobo.comparar(aire));
		assertEquals("lobo le gana a aire", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboGanaAAgua() {

		assertEquals(1, lobo.comparar(agua));
		assertEquals("lobo le gana a agua", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboGanaADragon() {

		assertEquals(1, lobo.comparar(dragon));
		assertEquals("lobo le gana a dragon", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboGanaADemonio() {

		assertEquals(1, lobo.comparar(demonio));
		assertEquals("lobo le gana a demonio", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboGanaARayo() {

		assertEquals(1, lobo.comparar(rayo));
		assertEquals("lobo le gana a rayo", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboPierdeConPistola() {

		assertEquals(-1, lobo.comparar(pistola));
		assertEquals("lobo le pierde a pistola", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboPierdeConPiedra() {

		assertEquals(-1, lobo.comparar(piedra));
		assertEquals("lobo le pierde a piedra", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboPierdeConFuego() {

		assertEquals(-1, lobo.comparar(fuego));
		assertEquals("lobo le pierde a fuego", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboPierdeConTijera() {

		assertEquals(-1, lobo.comparar(tijera));
		assertEquals("lobo le pierde a tijera", lobo.getDescripcionResultado().toLowerCase());
	}


	@Test
	void testCompararLoboPierdeConHumano() {

		assertEquals(-1, lobo.comparar(humano));
		assertEquals("lobo le pierde a humano", lobo.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararLoboPierdeConSerpiente() {

		assertEquals(-1, lobo.comparar(serpiente));
		assertEquals("lobo le pierde a serpiente", lobo.getDescripcionResultado().toLowerCase());
	}


	@Test
	void testCompararLoboEmpataConLobo() {

		assertEquals(0, lobo.comparar(lobo));
		assertEquals("lobo le empata a lobo", lobo.getDescripcionResultado()
													  .toLowerCase());

	}
	
	// Casos de Esponja
	
	// gana
	@Test
	void testCompararEsponjaGanaAPistola() {
		assertEquals(1, esponja.comparar(pistola));
		assertEquals("esponja le gana a pistola", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararEsponjaGanaARayo() {
		assertEquals(1, esponja.comparar(rayo));
		assertEquals("esponja le gana a rayo", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararEsponjaGanaDemonio() {
		assertEquals(1, esponja.comparar(demonio));
		assertEquals("esponja le gana a demonio", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararEsponjaGanaADragon() {
		assertEquals(1, esponja.comparar(dragon));
		assertEquals("esponja le gana a dragon", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararEsponjaGanaAgua() {
		assertEquals(1, esponja.comparar(agua));
		assertEquals("esponja le gana a agua", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararEsponjaGanaAire() {
		assertEquals(1, esponja.comparar(aire));
		assertEquals("esponja le gana a aire", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararEsponjaGanaAPapel() {
		assertEquals(1, esponja.comparar(papel));
		assertEquals("esponja le gana a papel", esponja.getDescripcionResultado()
													  .toLowerCase());
	}	
		
	
	// perdio
	
	@Test
	void testCompararEsponjaPierdeConLobo() {
		assertEquals(-1, esponja.comparar(lobo));
		assertEquals("esponja le pierde a lobo", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
			
	@Test
	void testCompararEsponjaPierdeConArbol() {
		assertEquals(-1, esponja.comparar(arbol));
		assertEquals("esponja le pierde a arbol", esponja.getDescripcionResultado()
													  .toLowerCase());		
	}
			
	@Test
	void testCompararEsponjaPierdeConHumano() {
		assertEquals(-1, esponja.comparar(humano));
		assertEquals("esponja le pierde a humano", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
			
	@Test
	void testCompararEsponjaPierdeConSerpiente() {
		assertEquals(-1, esponja.comparar(serpiente));
		assertEquals("esponja le pierde a serpiente", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
			
	@Test
	void testCompararEsponjaPierdeConTijeras() {
		assertEquals(-1, esponja.comparar(tijera));
		assertEquals("esponja le pierde a tijera", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
			
	@Test
	void testCompararEsponjaPierdeConFuego() {
		assertEquals(-1, esponja.comparar(fuego));
		assertEquals("esponja le pierde a fuego", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
			
	@Test
	void testCompararEsponjaPierdeConPiedra() {
		assertEquals(-1, esponja.comparar(piedra));
		assertEquals("esponja le pierde a piedra", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	// empato
	@Test
	void testCompararEsponjaEmpataConEsponja() {
		assertEquals(0, esponja.comparar(esponja));
		assertEquals("esponja le empata a esponja", esponja.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//CASOS ARBOL
	//ARBOL GANA CON...
	
	@Test
	void testCompararArbolGanaADemonio() {

		assertEquals(1, arbol.comparar(demonio));
		assertEquals("arbol le gana a demonio", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolGanaADragon() {

		assertEquals(1, arbol.comparar(dragon));
		assertEquals("arbol le gana a dragon", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolGanaAgua() {

		assertEquals(1, arbol.comparar(agua));
		assertEquals("arbol le gana a agua", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolGanaAire() {

		assertEquals(1, arbol.comparar(aire));
		assertEquals("arbol le gana a aire", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolGanaAPapel() {

		assertEquals(1, arbol.comparar(papel));
		assertEquals("arbol le gana a papel", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolGanaAEsponja() {

		assertEquals(1, arbol.comparar(esponja));
		assertEquals("arbol le gana a esponja", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolGanaALobo() {

		assertEquals(1, arbol.comparar(lobo));
		assertEquals("arbol le gana a lobo", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//ARBOL PIERDE CON ...
	
	@Test
	void testCompararArbolPierdeConRayo() {

		assertEquals(-1, arbol.comparar(rayo));
		assertEquals("arbol le pierde a rayo", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolPierdeConPistola() {

		assertEquals(-1, arbol.comparar(pistola));
		assertEquals("arbol le pierde a pistola", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolPierdeConPiedra() {

		assertEquals(-1, arbol.comparar(piedra));
		assertEquals("arbol le pierde a piedra", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolPierdeConFuego() {

		assertEquals(-1, arbol.comparar(fuego));
		assertEquals("arbol le pierde a fuego", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolPierdeConTijera() {

		assertEquals(-1, arbol.comparar(tijera));
		assertEquals("arbol le pierde a tijera", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolPierdeConSerpiente() {

		assertEquals(-1, arbol.comparar(serpiente));
		assertEquals("arbol le pierde a serpiente", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararArbolPierdeConHumano() {

		assertEquals(-1, arbol.comparar(humano));
		assertEquals("arbol le pierde a humano", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//ARBOL EMPATA
	@Test
	void testCompararArbolEmpataConArbol() {

		assertEquals(0, arbol.comparar(arbol));
		assertEquals("arbol le empata a arbol", arbol.getDescripcionResultado()
													  .toLowerCase());
	}
	
	// Casos de HUMANO

	@Test
	void testCompararHumanoGanaArbol() {

		assertEquals(1, humano.comparar(arbol));
		assertEquals("humano le gana a arbol", humano.getDescripcionResultado().toLowerCase());
	}
	
	@Test
	void testCompararHumanoGanaDragon() {

		assertEquals(1, humano.comparar(dragon));
		assertEquals("humano le gana a dragon", humano.getDescripcionResultado().toLowerCase());
	}

	
	
	@Test
	void testCompararHumanoGanaAgua() {

		assertEquals(1, humano.comparar(agua));
		assertEquals("humano le gana a agua", humano.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararHumanoGanaAire() {

		assertEquals(1, humano.comparar(aire));
		assertEquals("humano le gana a aire", humano.getDescripcionResultado().toLowerCase());
	}

	
	@Test
	void testCompararHumanoGanaPapel() {

		assertEquals(1, humano.comparar(papel));
		assertEquals("humano le gana a papel", humano.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararHumanoGanaEsponja() {

		assertEquals(1, humano.comparar(esponja));
		assertEquals("humano le gana a esponja", humano.getDescripcionResultado().toLowerCase());
	}
	
	@Test
	void testCompararHumanoGanaLobo() {

		assertEquals(1, humano.comparar(lobo));
		assertEquals("humano le gana a lobo", humano.getDescripcionResultado().toLowerCase());
	}




	@Test
	void testCompararHumanoPierdeConTijera() {
		assertEquals(-1, humano.comparar(tijera));
		assertEquals("humano le pierde a tijera", humano.getDescripcionResultado().toLowerCase());
	}	
	
	@Test
	void testCompararHumanoPierdeConFuego() {
		assertEquals(-1, humano.comparar(fuego));
		assertEquals("humano le pierde a fuego", humano.getDescripcionResultado().toLowerCase());
	}	
	
	@Test
	void testCompararHumanoPierdeConPiedra() {
		assertEquals(-1, humano.comparar(piedra));
		assertEquals("humano le pierde a piedra", humano.getDescripcionResultado().toLowerCase());
	}	
	
	@Test
	void testCompararHumanoPierdeConPistola() {
		assertEquals(-1, humano.comparar(pistola));
		assertEquals("humano le pierde a pistola", humano.getDescripcionResultado().toLowerCase());
	}	
	
	@Test
	void testCompararHumanoPierdeConRayo() {
		assertEquals(-1, humano.comparar(rayo));
		assertEquals("humano le pierde a rayo", humano.getDescripcionResultado().toLowerCase());
	}	
	
	@Test
	void testCompararHumanoPierdeConDemonio() {
		assertEquals(-1, humano.comparar(demonio));
		assertEquals("humano le pierde a demonio", humano.getDescripcionResultado().toLowerCase());
	}	
	
	@Test
	void testCompararHumanoPierdeConSerpiente() {
		assertEquals(-1, humano.comparar(serpiente));
		assertEquals("humano le pierde a serpiente", humano.getDescripcionResultado().toLowerCase());
	}	
	
	//HUMANO EMPATA
	@Test
	void testCompararHumanoEmpataConHumano() {

		assertEquals(0, humano.comparar(humano));
		assertEquals("humano le empata a humano", humano.getDescripcionResultado()
													  .toLowerCase());
	}

	// Casos fuego

	@Test
	void testCompararFuegoGanaATijera() {

		assertEquals(1, fuego.comparar(tijera));
		assertEquals("fuego le gana a tijera", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoGanaASerpiente() {

		assertEquals(1, fuego.comparar(serpiente));
		assertEquals("fuego le gana a serpiente", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoGanaAHumano() {

		assertEquals(1, fuego.comparar(humano));
		assertEquals("fuego le gana a humano", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoGanaAArbol() {

		assertEquals(1, fuego.comparar(arbol));
		assertEquals("fuego le gana a arbol", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoGanaALobo() {

		assertEquals(1, fuego.comparar(lobo));
		assertEquals("fuego le gana a lobo", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoGanaAEsponja() {

		assertEquals(1, fuego.comparar(esponja));
		assertEquals("fuego le gana a esponja", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoGanaAPapel() {

		assertEquals(1, fuego.comparar(papel));
		assertEquals("fuego le gana a papel", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoPierdeConAire() {

		assertEquals(-1, fuego.comparar(aire));
		assertEquals("fuego le pierde a aire", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoPierdeConAgua() {

		assertEquals(-1, fuego.comparar(agua));
		assertEquals("fuego le pierde a agua", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoPierdeConDragon() {

		assertEquals(-1, fuego.comparar(dragon));
		assertEquals("fuego le pierde a dragon", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoPierdeConDemonio() {

		assertEquals(-1, fuego.comparar(demonio));
		assertEquals("fuego le pierde a demonio", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoPierdeConRayo() {

		assertEquals(-1, fuego.comparar(rayo));
		assertEquals("fuego le pierde a rayo", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoPierdeConPistola() {

		assertEquals(-1, fuego.comparar(pistola));
		assertEquals("fuego le pierde a pistola", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoPierdeConPiedra() {

		assertEquals(-1, fuego.comparar(piedra));
		assertEquals("fuego le pierde a piedra", fuego.getDescripcionResultado().toLowerCase());
	}

	@Test
	void testCompararFuegoEmpataConFuego() {

		assertEquals(0, fuego.comparar(fuego));
		assertEquals("fuego le empata a fuego", fuego.getDescripcionResultado().toLowerCase());
	}

	
	// Casos de Serpiente
	
	// gana
		@Test
		void testCompararSerpienteGanaAHumano() {
			assertEquals(1, serpiente.comparar(humano));
			assertEquals("serpiente le gana a humano", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}
		
		@Test
		void testCompararSerpienteGanaArbol() {
			assertEquals(1, serpiente.comparar(arbol));
			assertEquals("serpiente le gana a arbol", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}
		
		@Test
		void testCompararSerpienteGanaALobo() {
			assertEquals(1, serpiente.comparar(lobo));
			assertEquals("serpiente le gana a lobo", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}
		
		@Test
		void testCompararSerpienteGanaAEsponja() {
			assertEquals(1, serpiente.comparar(esponja));
			assertEquals("serpiente le gana a esponja", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}
		
		@Test
		void testCompararSerpienteGanaAPapel() {
			assertEquals(1, serpiente.comparar(papel));
			assertEquals("serpiente le gana a papel", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}
		
		@Test
		void testCompararSerpienteGanaAire() {
			assertEquals(1, serpiente.comparar(aire));
			assertEquals("serpiente le gana a aire", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}
		
		@Test
		void testCompararSerpienteGanaAgua() {
			assertEquals(1, serpiente.comparar(agua));
			assertEquals("serpiente le gana a agua", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}	
			
		
		// perdio
		
		@Test
		void testCompararSerpientePierdeConTijeras() {
			assertEquals(-1, serpiente.comparar(tijera));
			assertEquals("serpiente le pierde a tijera", serpiente.getDescripcionResultado()
														  .toLowerCase());	
		}
				
		@Test
		void testCompararSerpientePierdeConFuego() {
			assertEquals(-1, serpiente.comparar(fuego));
			assertEquals("serpiente le pierde a fuego", serpiente.getDescripcionResultado()
														  .toLowerCase());		
		}
				
		@Test
		void testCompararSerpientePierdeConPiedra() {
			assertEquals(-1, serpiente.comparar(piedra));
			assertEquals("serpiente le pierde a piedra", serpiente.getDescripcionResultado()
														  .toLowerCase());	
		}
				
		@Test
		void testCompararSerpientePierdeConArma() {
			assertEquals(-1, serpiente.comparar(pistola));
			assertEquals("serpiente le pierde a pistola", serpiente.getDescripcionResultado()
														  .toLowerCase());	
		}
				
		@Test
		void testCompararSerpientePierdeConRayo() {
			assertEquals(-1, serpiente.comparar(rayo));
			assertEquals("serpiente le pierde a rayo", serpiente.getDescripcionResultado()
														  .toLowerCase());	
		}
				
		@Test
		void testCompararSerpientePierdeConDemonio() {
			assertEquals(-1, serpiente.comparar(demonio));
			assertEquals("serpiente le pierde a demonio", serpiente.getDescripcionResultado()
														  .toLowerCase());	
		}
				
		@Test
		void testCompararSerpientePierdeConDragon() {
			assertEquals(-1, serpiente.comparar(dragon));
			assertEquals("serpiente le pierde a dragon", serpiente.getDescripcionResultado()
														  .toLowerCase());	
		}
	
		// empato
		@Test
		void testCompararSerpienteEmpataConSerpiente() {
			assertEquals(0, serpiente.comparar(serpiente));
			assertEquals("serpiente le empata a serpiente", serpiente.getDescripcionResultado()
														  .toLowerCase());
		}	
	
}
