package ar.edu.proyectoFinalJuego.modelo;

public class Demonio extends PiedraPapelTijeraFactory {
	public Demonio() {
		this("demonio", DEMONIO);
	}
	
	public Demonio(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==DEMONIO;
	}
  
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
			case RAYO:
			case PISTOLA:
			case PIEDRA:
			case TIJERA:
			case SERPIENTE:
			case HUMANO:
			case FUEGO:
				resul=1;
				this.descripcionResultado = "demonio le gana a " + pPiedPapelTijera.getNombre();
			break;
			
	        case ARBOL:
	        case LOBO:
	        case ESPONJA:
	        case PAPEL:
	        case AIRE:
	        case AGUA:
	        case DRAGON:
	        	resul=-1;
	        	this.descripcionResultado = "demonio le pierde a " + pPiedPapelTijera.getNombre();
			break;

	        default:
	        	resul=0;
	        	this.descripcionResultado = "demonio le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
