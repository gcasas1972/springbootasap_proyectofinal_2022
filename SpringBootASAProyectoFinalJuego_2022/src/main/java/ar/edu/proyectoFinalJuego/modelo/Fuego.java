package ar.edu.proyectoFinalJuego.modelo;

public class Fuego extends PiedraPapelTijeraFactory {
	public Fuego() {
		this("fuego", FUEGO);
	}
	
	public Fuego(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==FUEGO;
	}
  
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
			case TIJERA:
			case SERPIENTE:
			case HUMANO:
			case ARBOL:
			case LOBO:
			case ESPONJA:
			case PAPEL:
				resul=1;
				this.descripcionResultado = "fuego le gana a " + pPiedPapelTijera.getNombre();
			break;
			
	        case AIRE:
	        case AGUA:
	        case DRAGON:
	        case DEMONIO:
	        case RAYO:
	        case PISTOLA:
	        case PIEDRA:
	        	resul=-1;
	        	this.descripcionResultado = "fuego le pierde a " + pPiedPapelTijera.getNombre();
			break;

	        default:
	        	resul=0;
	        	this.descripcionResultado = "fuego le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
