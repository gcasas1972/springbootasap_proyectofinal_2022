package ar.edu.proyectoFinalJuego.modelo;

public class Papel extends PiedraPapelTijeraFactory {
	public Papel() {
		this("papel", PAPEL);
	}
	public Papel(String pNom, int pNum) {
		super(pNom,pNum);
		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==PAPEL;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		//le gana 
		case PIEDRA:
		case PISTOLA:
		case RAYO:
		case DEMONIO:
		case DRAGON:
		case AGUA:
		case AIRE:
			resul=1;
			this.descripcionResultado = "Papel le gana a " + pPiedPapelTijera.getNombre();
			break ;
     //pierde con
		case TIJERA:
		case FUEGO:
		case SERPIENTE:
		case HUMANO:
		case ARBOL:
		case LOBO:
		case ESPONJA:
			resul=-1;
			this.descripcionResultado = "Papel le pierde a " + pPiedPapelTijera.getNombre();
			break;
		default:			
			resul=0;
			this.descripcionResultado = "Papel le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
