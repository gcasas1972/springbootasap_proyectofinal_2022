package ar.edu.proyectoFinalJuego.modelo;

public class Dragon extends PiedraPapelTijeraFactory {
	
	public Dragon() {
		this("dragon", DRAGON);
	}
	
	public Dragon(String pNom, int pNum) {
		super(pNom,pNum);
		
	}

	@Override
	public boolean isMe(int pNum) {
		// TODO Auto-generated method stub
		return pNum == DRAGON;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO Auto-generated method stub
		int resul = 0;
		switch (pPiedPapelTijera.getNumero()) {
			//le gana a
			case DEMONIO:
			case RAYO:
			case PISTOLA:
			case PIEDRA:
			case FUEGO:
			case TIJERA:
			case SERPIENTE:
				resul=1;
				this.descripcionResultado = "Dragon le gana a " + pPiedPapelTijera.getNombre();
				break ;
			//pierde con
			case HUMANO:
			case ARBOL:
			case LOBO:
			case ESPONJA:
			case PAPEL:
			case AIRE:
			case AGUA:
				resul=-1;
				this.descripcionResultado = "Dragon le pierde a " + pPiedPapelTijera.getNombre();
				break;
			default:			
				resul=0;
				this.descripcionResultado = "Dragon le empata a " + pPiedPapelTijera.getNombre();
				break;
			}
			return resul;
		}

}
