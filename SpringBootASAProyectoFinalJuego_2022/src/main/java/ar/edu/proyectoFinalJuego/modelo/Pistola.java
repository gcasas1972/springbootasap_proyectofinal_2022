package ar.edu.proyectoFinalJuego.modelo;

public class Pistola extends PiedraPapelTijeraFactory {
	public Pistola() {
		this("pistola",PISTOLA );
	}
	
	public Pistola(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==PISTOLA;
	}
  
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
			case LOBO:
			case ARBOL:
			case HUMANO:
			case SERPIENTE:
			case TIJERA:
			case FUEGO:
			case PIEDRA:			
				resul=1;
				this.descripcionResultado = "pistola le gana a " + pPiedPapelTijera.getNombre();
			break;
			
			case RAYO:
			case DEMONIO:
			case DRAGON:
			case AGUA:
			case AIRE:
			case PAPEL:
			case ESPONJA:
				resul=-1;
				this.descripcionResultado = "pistola le pierde a " + pPiedPapelTijera.getNombre();
				break;
	        default:
				resul=0;
				this.descripcionResultado = "pistola le empata a " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

	

}