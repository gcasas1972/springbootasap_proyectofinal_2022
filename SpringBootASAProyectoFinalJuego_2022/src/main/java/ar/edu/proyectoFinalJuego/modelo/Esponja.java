package ar.edu.proyectoFinalJuego.modelo;

public class Esponja extends PiedraPapelTijeraFactory {
	public Esponja() {
		this("esponja", ESPONJA);
	}
	
	public Esponja(String pNom, int pNum) {
		super(pNom,pNum);
		
	}

	@Override
	public boolean isMe(int pNum) {
		// TODO Auto-generated method stub
		return pNum == ESPONJA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO Auto-generated method stub
		int resul = 0;
		switch (pPiedPapelTijera.getNumero()) {
			//le gana a
			case PISTOLA:
			case RAYO:
			case DEMONIO:
			case DRAGON:
			case AGUA:
			case AIRE:
			case PAPEL:
				resul=1;
				this.descripcionResultado = "Esponja le gana a " + pPiedPapelTijera.getNombre();
				break ;
			//pierde con
			case LOBO:
			case ARBOL:
			case HUMANO:
			case SERPIENTE:
			case TIJERA:
			case FUEGO:
			case PIEDRA:
				resul=-1;
				this.descripcionResultado ="Esponja le pierde a " + pPiedPapelTijera.getNombre();
				break;
			default:			
				resul=0;
				this.descripcionResultado = "Esponja le empata a " + pPiedPapelTijera.getNombre();
				break;
		}
		return resul;
	}
}
