-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.13-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema asap_pokemon
--

CREATE DATABASE IF NOT EXISTS asap_pokemon;
USE asap_pokemon;

--
-- Definition of table `jugadas`
--

DROP TABLE IF EXISTS `jugadas`;
CREATE TABLE `jugadas` (
  `JDA_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JUG1_ID` int(10) unsigned NOT NULL,
  `JDA1_NRO` int(10) unsigned NOT NULL,
  `JUG2_ID` int(10) unsigned NOT NULL,
  `JUG2_NRO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`JDA_ID`),
  KEY `FK_jugadas_jugador1` (`JUG1_ID`),
  KEY `FK_jugadas_jugador2` (`JUG2_ID`),
  CONSTRAINT `FK_jugadas_jugador1` FOREIGN KEY (`JUG1_ID`) REFERENCES `jugadores` (`JUG_ID`),
  CONSTRAINT `FK_jugadas_jugador2` FOREIGN KEY (`JUG2_ID`) REFERENCES `jugadores` (`JUG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jugadas`
--

/*!40000 ALTER TABLE `jugadas` DISABLE KEYS */;
/*!40000 ALTER TABLE `jugadas` ENABLE KEYS */;


--
-- Definition of table `jugadores`
--

DROP TABLE IF EXISTS `jugadores`;
CREATE TABLE `jugadores` (
  `JUG_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JUG_NOMBRE` varchar(45) NOT NULL,
  `JUG_APELLIDO` varchar(45) NOT NULL,
  `JUG_NICKNAME` varchar(45) NOT NULL,
  PRIMARY KEY (`JUG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jugadores`
--

/*!40000 ALTER TABLE `jugadores` DISABLE KEYS */;
/*!40000 ALTER TABLE `jugadores` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
